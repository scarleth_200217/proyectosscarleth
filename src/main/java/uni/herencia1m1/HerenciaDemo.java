/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni.herencia1m1;

import uni.pojo.Empleado;
import uni.pojo.Estudiante;
import uni.pojo.Persona;

/**
 *
 * @author Sistemas39
 */
public class HerenciaDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Persona[] personas = {
                new Empleado (1, " Ing de transporte terrestre",
                "Pepito", "Perez"),
                new Estudiante ("2019-0000U", "Ana", "Conda") 
        };
       for (Persona p : personas){
           print(p);
       }

    }
    
    public static void print(Persona e){
        System.out.println(e.toString());
    }
}
