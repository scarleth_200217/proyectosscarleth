/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni.pojo;

import java.util.Objects;

/**
 *
 * @author Sistemas39
 */
public class Empleado extends Persona{
    private int codigo;
    private String cargo;

    public Empleado(int codigo, String cargo, String nombres, String apellidos) {
        super(nombres, apellidos);
        this.codigo = codigo;
        this.cargo = cargo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
    
   
    @Override
    public String toString() {
        return "Empleado{" + 
                "codigo: " + codigo + 
                ", cargo: " + cargo + 
                ", nombres: " + nombres +
                ", apellidos: " + apellidos +
                '}';
         
    }
    
}
